﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Sketch;
using System.Web.Security;
using Sketch.Models;
using Microsoft.AspNet.Identity;
namespace Sketch.Controllers 
{ 
    public class BoardController : Controller
    {
        private ChalkifiEntities1 db = new ChalkifiEntities1();

        // GET: Board/BoardIndex
        public ActionResult BoardIndex()
        {

            var userId = HttpContext.User.Identity.GetUserId();
            var board = db.Boards.Where(c=>c.Instructor==userId)
                .Include(b => b.AspNetUser);
            return View(board.ToList());
        }

        /*
        // GET: Board/BoardDetails/5
        public ActionResult BoardDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Board board = db.Boards.Find(id);
            if (board == null)
            {
                return HttpNotFound();
            }
            return View(board);
        }
        */

        // GET: Board/BoardCreate
        public ActionResult BoardCreate()
        {
            ViewBag.Instructor = new SelectList(db.AspNetUsers, "Id", "UserName");
            return View();
        }

        // POST: Board/BoardCreate
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult BoardCreate([Bind(Include = "Id,Duration,startdate,Instructor,Description,Name,BoardFile,AudioFile,Finished,AspNetUser")] Board board)
        {
            if (ModelState.IsValid)
            {
                board.Instructor = User.Identity.GetUserId();
                db.Boards.Add(board);
                db.SaveChanges();
                DisplaySuccessMessage("Has append a Board record");
                return RedirectToAction("BoardIndex");
            }

            ViewBag.Instructor = new SelectList(db.AspNetUsers, "Id", "UserName", board.Instructor);
            DisplayErrorMessage();
            return View(board);
        }

        // GET: Board/BoardEdit/5
        public ActionResult BoardEdit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Board board = db.Boards.Find(id);
            if (board == null)
            {
                return HttpNotFound();
            }
            ViewBag.Instructor = new SelectList(db.AspNetUsers, "Id", "UserName", board.Instructor);
            return View(board);
        }

        // POST: BoardBoard/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult BoardEdit([Bind(Include = "Id,Duration,startdate,Instructor,Description,Name,BoardFile,AudioFile,Finished,AspNetUser")] Board board)
        {
            if (ModelState.IsValid)
            {
                board.Instructor = User.Identity.GetUserId();
                var oldBoard = db.Boards.First(c => c.Id == board.Id);
                board.Finished = oldBoard.Finished;
                board.AudioFile = oldBoard.AudioFile;
                board.BoardFile = oldBoard.BoardFile;
                board.Instructor = oldBoard.Instructor;
              //  db.Entry(board).State = EntityState.Modified;
                db.SaveChanges();
                DisplaySuccessMessage("Has update a Board record");
                return RedirectToAction("BoardIndex");
            }
            ViewBag.Instructor = new SelectList(db.AspNetUsers, "Id", "UserName", board.Instructor);
            DisplayErrorMessage();
            return View(board);
        }

        // GET: Board/BoardDelete/5
        public ActionResult BoardDelete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Board board = db.Boards.Find(id);
            if (board == null)
            {
                return HttpNotFound();
            }
            return View(board);
        }

        // POST: Board/BoardDelete/5
        [HttpPost, ActionName("BoardDelete")]
        [ValidateAntiForgeryToken]
        public ActionResult BoardDeleteConfirmed(int id)
        {
            Board board = db.Boards.Find(id);
            db.Boards.Remove(board);
            db.SaveChanges();
            DisplaySuccessMessage("Has delete a Board record");
            return RedirectToAction("BoardIndex");
        }

        private void DisplaySuccessMessage(string msgText)
        {
            TempData["SuccessMessage"] = msgText;
        }

        private void DisplayErrorMessage()
        {
            TempData["ErrorMessage"] = "Save changes was unsuccessful.";
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
