﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sketch.Models
{
    public class CanvasBoard
    {
        public string Iframe{get;set;}
        public string Type{get;set;}
       public string SlideId { get; set; }
       public int room { get; set; }
        public string initCanvas { get; set; }
        public List<string> SlideHistory { get; set; }
         
    }
}