﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sketch.Models
{
    public struct Path
    {
        public int x { get; set; }
        public int y { get; set; }
    }
}