﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Sketch
{
    [MetadataType(typeof(BoardMetadata))]
    public partial class Board
    {
    }

    public partial class BoardMetadata
    {
        [Required(ErrorMessage = "Please enter : Id")]
        [Display(Name = "Id")]
        public int Id { get; set; }

        [Display(Name = "Duration")]
        public int Duration { get; set; }

        [Display(Name = "startdate")]
        public DateTime startdate { get; set; }

        [Display(Name = "Instructor")]
        public string Instructor { get; set; }

        [Display(Name = "Description")]
        public string Description { get; set; }

        [Display(Name = "Name")]
        public string Name { get; set; }

        [Display(Name = "BoardFile")]
        public string BoardFile { get; set; }

        [Display(Name = "AudioFile")]
        public string AudioFile { get; set; }

        [Display(Name = "Finished")]
        public bool Finished { get; set; }

        [Display(Name = "AspNetUser")]
        public AspNetUser AspNetUser { get; set; }

    }
}
