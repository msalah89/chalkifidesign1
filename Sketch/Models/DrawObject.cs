﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sketch.Models
{
    public class DrawObject
    {
        public int x { get; set; }
        public int y { get; set; }
        public int w { get; set; }
        public int h { get; set; }
        public int index { get; set; }

        public string fill { get; set; }
        public string tool { get; set; }
        public string stroke { get; set; }
        public bool canresize { get; set; }
        public bool deleted { get; set; }
        public dynamic paths { get; set; }
    }
}